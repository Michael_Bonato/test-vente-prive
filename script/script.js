var app = new Vue({
	el:'#app',
	data: {
		countries: [
		{tag: "all", name: "destination"},
		{tag: "emirates", name: "Emirates Arabes Unis"},
		{tag: "maurice", name: "Maurice"},
		{tag: "indonesie", name: "Indonésie"},
		{tag: "maldives", name: "Maldives"},
		{tag: "thailande", name: "Thaïlande"},
		{tag: "srilanka", name: "Sri Lanka"},
		{tag: "japon", name: "Japon"},
		{tag: "vietnam", name: "Vietnam"}
		],
		details: [
			{
				detailID: "1",
				detailClass: "emirates",
				image: [
					{number: 1, tag:"image--one"}, 
					{number: 2, tag:"image--two"}, 
					{number: 3, tag:"image--three"}
				],
				reduction: "Dès 67€",
				destination: "Émirats Arabes Unis - Dubaï",
				hotel: "Hyatt Regency Creek",
				star: 5,
				black: "premium",
				gold: "Surclassement offert"
			},
			{
				detailID: "2",
				detailClass: "maurice",
				image: [
					{number: 1, tag:"image--four"}, 
					{number: 2, tag:"image--five"}, 
					{number: 3, tag:"image--six"}
				],
				reduction: "Jusqu'à -64%",
				destination: "Maurice - Grand Rivière",
				hotel: "Laguna Beach Hotel & Spa",
				star: 4,
				black: "Tout inclus",
				gold: "spa"
			},
			{
				detailID: "3",
				detailClass: "emirates",
				image: [
					{number: 1, tag:"image--height"}, 
					{number: 2, tag:"image--nine"}, 
					{number: 3, tag:"image--ten"}
				],
				reduction: "Dès 99€",
				destination: "Emirates Arabes Unis - Dubaï",
				hotel: "Fairmont Dubaï",
				star: 5,
				black: "Premium",
				gold: "Rooftop"
			},
			{
				detailID: "4",
				detailClass: "indonesie",
				image: [
					{number: 1, tag:"image--ten"}, 
					{number: 2, tag:"image--two"}, 
					{number: 3, tag:"image--four"}
				],
				reduction: "Dès 469€",
				destination: "Indonésie - Bali & Gili",
				hotel: "Combiné Sthala - Marc - Patra",
				star: 0,
				black: "Combiné",
				gold: "Petit déjeuner inclus"
			},
			{
				detailID: "5",
				detailClass: "maldives",
				image: [
					{number: 1, tag:"image--one"}, 
					{number: 2, tag:"image--three"}, 
					{number: 3, tag:"image--five"}
				],
				reduction: "Jusqu'à -36%",
				destination: "Maldives - Atoll de Noonu",
				hotel: "Noku Maldives",
				star: 5,
				black: "Premium",
				gold: "Massage offert"
			},
			{
				detailID: "6",
				detailClass: "thailande",
				image: [
					{number: 1, tag:"image--five"}, 
					{number: 2, tag:"image--nine"}, 
					{number: 3, tag:"image--three"}
				],
				reduction: "Jusqu'à -70%",
				destination: "Thaïlande - Impiana Resort Samui",
				hotel: "Impiana Resort Samui",
				star: 4,
				black: "Massage offert",
				gold: "Surclassement offert"
			},
			{
				detailID: "7",
				detailClass: "maurice",
				image: [
					{number: 1, tag:"image--six"}, 
					{number: 2, tag:"image--height"}, 
					{number: 3, tag:"image--ten"}
				],
				reduction: "Dès 538€",
				destination: "Maurice - Grand Baie",
				hotel: "Clos du Littoral",
				star: 4,
				black: "Premium",
				gold: "Villas avec Piscine Privée"
			},
			{
				detailID: "8",
				detailClass: "srilanka",
				image: [
					{number: 1, tag:"image--four"}, 
					{number: 2, tag:"image--height"}, 
					{number: 3, tag:"image--two"}
				],
				reduction: "Dès 699€",
				destination: "Sri Lanka - Sri Lanka",
				hotel: "Echappée SriLankaise",
				star: 3,
				black: "Circuit",
				gold: "Privatif"
			},
			{
				detailID: "9",
				detailClass: "japon",
				image: [
					{number: 1, tag:"image--six"}, 
					{number: 2, tag:"image--nine"}, 
					{number: 3, tag:"image--three"}
				],
				reduction: "Dès 114€",
				destination: "Japon - Tokyo",
				hotel: "Grand Arc Hanzomon",
				star: 3,
				black: "City Break",
				gold: "Insolite"
			},
			{
				detailID: "10",
				detailClass: "vietnam",
				image: [
					{number: 1, tag:"image--four"}, 
					{number: 2, tag:"image--ten"}, 
					{number: 3, tag:"image--five"}
				],
				reduction: "Dès 779€",
				destination: "Vietnam - De Hanoï à Hoi An",
				hotel: "Entre Culture et Plages",
				star: 4,
				black: "Circuit",
				gold: "Privatif"
			}
		]
	},
	methods: {

	}
})


var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });


$( document ).ready(function() {
	var country = $('#country__selector');
	country.change(function(){
		if ($(this).val() == 'all'){
			$('.offer--block').show();
		} else {
			$('.offer--block').hide();
			$block = $(this).val();
			$('.'+$block ).show();
		};
	});
});